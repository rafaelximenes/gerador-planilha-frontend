FROM nginx:alpine

COPY nginx/default.conf /etc/nginx/conf.d/default.conf
RUN rm -f /usr/share/nginx/html/index.html
RUN rm -f /usr/share/nginx/html/50x.html
COPY dist/ /usr/share/nginx/html/
RUN chown -R nginx /etc/nginx
RUN apk add tzdata
RUN cp /usr/share/zoneinfo/Brazil/East /etc/localtime
RUN apk del tzdata
EXPOSE 80
