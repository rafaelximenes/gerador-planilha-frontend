import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Planilha } from "./planilha.model";


@Injectable()
export class UserService {

    constructor(private httpClient: HttpClient) {

    }

    postFile(fileToUpload: File): Observable<boolean> {
        const endpoint = 'http://10.23.11.159:10001/planilha/api/upload';
        const formData: FormData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        return this.httpClient
          .post(endpoint, formData,
            {
                observe: 'response',
                responseType: 'text'
            }
    
             //{ headers: "Access-Control-Allow-Origin:" }
             )
          .map(() => { return true; });
    }

    findAll(): Observable<Planilha[]> {
        
        return this.httpClient.get<Planilha[]>(`http://10.23.11.159:10001/planilha`)
    }


}