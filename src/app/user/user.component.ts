import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';

@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit{

    msg: string
    download: boolean = false

    constructor(private fileService: UserService) {

    }
    fileToUpload: File = null;

    ngOnInit(){
    }

    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
    }

    uploadFileToActivity() {
        this.download=false
        this.msg = ""
        console.log("1")
        this.fileService.postFile(this.fileToUpload).subscribe(data => {
            console.log("deu certo")
            console.log(data)
            this.msg = "Arquivo processado com sucesso!"
            this.download=true
          // do something, if upload success
          }, error => {
            console.log("deu erro")
            console.log(error);
            this.msg = "Erro ao processar arquivo"
          });
      }

      downloadArquivo() {
          console.log("opa")
        return `http://10.23.11.159:10001/planilha/api/file`
      }
}
