import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from 'app/user/user.service';
import { Planilha } from 'app/user/planilha.model';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'table-cmp',
    moduleId: module.id,
    templateUrl: 'table.component.html'
})

export class TableComponent implements OnInit{
    public tableData1: TableData;
    public tableData2: TableData;
    public tableData3: TableData;

    planilha: Planilha[]

    constructor(private userServico: UserService) {
        this.userServico.findAll()
        .subscribe(response => {
            this.planilha = response;    
        })
    }

    retornaLink(row): string{
        console.log(row)
        console.log(row[0])
        return `http://10.23.11.159:10001/planilha/api/file?data=${row[0]}&codigoAcesso=${row[1]}`
    }

    private teste(): string[][] {
        console.log("entrou no teste")
        let a: Array<string[]> = new Array
        for(let i=0;i<this.planilha.length;i++) {
            console.log("2")
            let aux: string =""
            if(this.planilha[i].dataInsercao!=null) {
                aux = this.planilha[i].dataInsercao.substring(8,10)+"/"+this.planilha[i].dataInsercao.substring(5,7)+"/"+this.planilha[i].dataInsercao.substring(0,4)
            }
            a.push([this.planilha[i].dataInsercao, this.planilha[i].codigoAcesso, 'link'])
        }
        console.log(a)
        return a;
    }

    ngOnInit(){
        this.userServico.findAll()
        .subscribe(response => {
            this.planilha = response;  
            console.log(this.planilha)
            this.tableData3 = {
                headerRow: [ 'Data Processamento', 'Código Acesso', 'Download'],
                dataRows: this.teste()
            };  
        })

        // this.tableData1 = {
        //     headerRow: [ 'ID', 'Name', 'Country', 'City', 'Salary'],
        //     dataRows: [
        //         ['1', 'Dakota Rice', 'Niger', 'Oud-Turnhout', '$36,738'],
        //         ['2', 'Minerva Hooper', 'Curaçao', 'Sinaai-Waas', '$23,789'],
        //         ['3', 'Sage Rodriguez', 'Netherlands', 'Baileux', '$56,142'],
        //         ['4', 'Philip Chaney', 'Korea, South', 'Overland Park', '$38,735'],
        //         ['5', 'Doris Greene', 'Malawi', 'Feldkirchen in Kärnten', '$63,542'],
        //         ['6', 'Mason Porter', 'Chile', 'Gloucester', '$78,615']
        //     ]
        // };
        // this.tableData2 = {
        //     headerRow: [ 'ID', 'Name',  'Salary', 'Country', 'City' ],
        //     dataRows: [
        //         ['1', 'Dakota Rice','$36,738', 'Niger', 'Oud-Turnhout' ],
        //         ['2', 'Minerva Hooper', '$23,789', 'Curaçao', 'Sinaai-Waas'],
        //         ['3', 'Sage Rodriguez', '$56,142', 'Netherlands', 'Baileux' ],
        //         ['4', 'Philip Chaney', '$38,735', 'Korea, South', 'Overland Park' ],
        //         ['5', 'Doris Greene', '$63,542', 'Malawi', 'Feldkirchen in Kärnten', ],
        //         ['6', 'Mason Porter', '$78,615', 'Chile', 'Gloucester' ]
        //     ]
        // };
        
    }

    onchangeNome(nome) {
        console.log(nome)
        // this.diarioService.findByFilters(this.dataString, this.nomeDiarioFiltro)
        //   .subscribe(response => {
        //     this.diarios = response;
        //   })
      }
    
}
